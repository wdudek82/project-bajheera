# Project Bajheera

## Table of contents

## 0. URLs

Frontend - http://localhost:4200
Swagger - http://localhost:4200/api/swagger/index.html

## 1. Description

Project Bajheera is an issue tracker web application.

## 1. Services

- Dashboard-Frontend - Frontend created in Angular
- Dashboard-Backend - Backend created in NestJS

## 2. Setup and Run

### 2.1. Development

```bash
$ docker-compose build [--no-cache] [dashboard-backend|dashboard-frontend|postgres|pgadmin]
```

```bash
$ docker-compose up [-d][-V]
```

The Dashboard Frontend is accessible on port: `14200`.

The Dashboard Backend is accessible on port/route: `14200/app`.

If direct access to frontend or backend is needed, add `"ports"`sections in `docker-compose.yml`in services in which it's required.

#### 2.1.1. Updating node_modules

Because node_modules are kept in anonymous volumes they won't be updated when image it rebuilt (even with `--no-cache`).

To refresh image `node_modules`:

1. rebuild image that had node dependencies have changed,
2. start project (`docker-compose up`) with additional option `-V | --renew-anon-volumes`.
   **!!ACHTUNT!!** this operation will **completely erase** current database.
   If you want to preserve the data, create a dump and load it after a refresh.

More details in the official [docs]("https://docs.docker.com/compose/reference/up/").

#### 2.1.2 Loading fixtures

To load initial dev/test data to the databasa use npm command:

```bash
$ npm run fixtures
```

#### 2.1.3. To clear Database

This can be done by complete removal of the database container

```bash
$ docker-compose rm postgres
```

because it will be restored on the next startup.

Similar effect can be achieved by passing `-V | --renew-anon-volumes` parameter when project is started:

```bash
$ docker-compose up -V
```

#### 2.1.2. Connect to pgAdmin dashboard

- go to `localhost:8000`,
- username: admin, password: admin123,
- click "Add New Server", and in "Connection" tab type:
  - host: "postgres"
  - username: "postgres"
  - password: "admin123"

### 2.2. Production

Set application version env variable (check VERSION.md)

```bash
  $ export APP_VER=v1.0.0
```

```bash
  $ docker-compose -f docker-compose-prod.yml up
```

## 3. Version Control

### 3.1. Add pre-commit

This tool will check formatting of the code when commit is attempted.
You will find full instruction how to install it here: https://pre-commit.com/

Short version:

1. install pre-commit: `pip install pre-commit`
2. add `.pre-commit-config.yaml` in the project root
3. install the git hook script: `pre-commit install`

### 3.2. Git naming conventions

#### 3.2.1. Branches

Format: `prefix/issue_id-issue_name`

Example: `maintenance/PB-20-Dockerize-all-services`

##### 3.2.1.1. Branch name prefixes

- `epic/` - branch for an epic task to which all its subtask branches will be merged
- `feature/` - a new feature implementation
- `bugfix/` - branch containing bugfix that will go through the normal flow of review and deployment
- `hotfix/` - branch containing fix that is needed immediately (only in case of an emergency)
- `maintenance/` - fixing typos, trivial clean up (e.g. old files deletion), updating dependencies or configurations

##### 3.2.1.2. Issue id

In most cases this will correspond to the issue id (project code and issue number).

#### 3.2.2. An anonymous branch or commit

If there is no corresponding task at the time of a branch creation, issue number is 0,
so correct branch name in such a case would be e.g.: `prefix/PB-0-branch-name`,
and for an anonymous commit it would be: `PB-0: commit message`

#### 3.2.3. Commit

Format `issue_id: commit message`

Example: `PB-20: add backend Dockerfile`

## 4. Testing

### 4.1. Backend

Jest watch mode requires git, so to be able to use jest watch mode (`npm run test:watch`) in the development container,
you need to create an empty git repository in the service root dir.
This empty .git affect root level repository and will be ignored in commits.
