#!/usr/bin/env bash

# hack that allows to avoid error MSB3021: Unable to copy file /app/API/obj/Debug to bin/Debug
dotnet clean > /dev/null

MIGRATIONS_DIR=Data/Migrations

if [[ $1 == "migrations" && $2 == "add" ]]; then
  dotnet-ef -p API "$@" --output-dir $MIGRATIONS_DIR
else
  dotnet-ef -p API "$@"
fi
