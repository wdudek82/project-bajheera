import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./components/home/home.component";
import { PageNotFoundComponent } from "./components/page-not-found/page-not-found.component";
import { MemberListComponent } from "./components/members/member-list/member-list.component";
import { MemberDetailComponent } from "./components/members/member-detail/member-detail.component";
import { ListsComponent } from "./components/lists/lists.component";
import { MessagesComponent } from "./components/messages/messages.component";
import { AuthGuard } from "./shared/guards/auth.guard";

const routes: Routes = [
  { path: "", component: HomeComponent },
  {
    path: "",
    runGuardsAndResolvers: "always",
    canActivate: [AuthGuard],
    children: [
      {
        path: "members",
        children: [
          { path: "", component: MemberListComponent },
          { path: ":id", component: MemberDetailComponent },
        ],
      },
      { path: "lists", component: ListsComponent },
      { path: "messages", component: MessagesComponent },
    ],
  },
  { path: "**", component: PageNotFoundComponent, pathMatch: "full" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
