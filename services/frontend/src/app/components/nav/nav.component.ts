import { Component, OnInit } from "@angular/core";
import { AccountService } from "../../shared/services/account.service";
import { LoginCredentials } from "../../shared/models/login-credentials";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-nav",
  templateUrl: "./nav.component.html",
  styleUrls: ["./nav.component.scss"],
})
export class NavComponent implements OnInit {
  credentials: LoginCredentials = {
    username: "",
    password: "",
  };

  constructor(
    public accountService: AccountService,
    private router: Router,
    private toastr: ToastrService,
  ) {}

  ngOnInit(): void {}

  login() {
    this.accountService.login(this.credentials).subscribe(
      (res) => {
        this.router.navigateByUrl("/members");
      },
      (error) => {
        console.error(error);
        this.toastr.error(error.error);
      },
    );
  }

  logout(): void {
    this.accountService.logout();
    this.router.navigateByUrl("/");
  }
}
