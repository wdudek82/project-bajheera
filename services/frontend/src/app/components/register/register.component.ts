import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { LoginCredentials } from "../../shared/models/login-credentials";
import { User } from "../../shared/models/user";
import { AccountService } from "../../shared/services/account.service";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.scss"],
})
export class RegisterComponent implements OnInit {
  @Output() cancelRegister = new EventEmitter<void>();
  credentials: LoginCredentials = {
    username: "",
    password: "",
  };

  constructor(
    private accountService: AccountService,
    private toastr: ToastrService,
  ) {}

  ngOnInit(): void {}

  register(): void {
    this.accountService.register(this.credentials).subscribe(
      (currentUser) => {
        console.log(currentUser);
        this.cancel();
      },
      (error) => {
        this.toastr.error(error.error);
      },
    );
  }

  cancel(): void {
    this.cancelRegister.emit();
  }
}
