import { Component, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Subscription } from "rxjs";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-member-detail",
  templateUrl: "./member-detail.component.html",
  styleUrls: ["./member-detail.component.scss"],
})
export class MemberDetailComponent implements OnInit, OnDestroy {
  id?: number;
  routeSub: Subscription = new Subscription();

  constructor(private route: ActivatedRoute, private toastr: ToastrService) {}

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe((params) => {
      console.log(params);
      this.id = params["id"];
    });
  }

  onClick() {
    this.toastr.success("Great Success!", "Members!");
  }

  ngOnDestroy(): void {
    this.routeSub.unsubscribe();
  }
}
