export interface User {
  id: number;
  userName: string;
  email: string;
  createdAt: string;
  updatedAt: string;
  deletedAt: string;
}
