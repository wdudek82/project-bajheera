import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, ReplaySubject } from "rxjs";
import { LoginCredentials } from "../models/login-credentials";
import { map, tap } from "rxjs/operators";
import { CurrentUser } from "../models/current-user";

@Injectable({
  providedIn: "root",
})
export class AccountService {
  private userSubject = new ReplaySubject<CurrentUser | null>(1);
  currentUser$: Observable<CurrentUser | null> = this.userSubject.asObservable();

  constructor(private http: HttpClient) {}

  login(credentials: LoginCredentials): Observable<CurrentUser> {
    return this.http.post<CurrentUser>(`/api/account/login`, credentials).pipe(
      tap((res) => {
        if (res) {
          localStorage.setItem("user", JSON.stringify(res));
          this.setCurrentUser(res);
        }
      }),
    );
  }

  getCurrentUser(): CurrentUser | null {
    const storedCurrentUser: string | null = localStorage.getItem("user");

    let user: CurrentUser | null = null;
    if (storedCurrentUser) {
      user = JSON.parse(storedCurrentUser);
    }

    return user;
  }

  setCurrentUser(user: CurrentUser | null): void {
    this.userSubject.next(user);

    if (user) {
      localStorage.setItem("user", JSON.stringify(user));
    } else {
      localStorage.removeItem("user");
    }
  }

  logout(): void {
    this.setCurrentUser(null);
  }

  register(credentials: LoginCredentials): Observable<CurrentUser> {
    return this.http
      .post<CurrentUser>("/api/account/register", credentials)
      .pipe(
        map((user) => {
          if (user) {
            this.setCurrentUser(user);
          }

          return user;
        }),
      );
  }
}
