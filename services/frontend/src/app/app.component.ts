import { Component, OnInit } from "@angular/core";
import { CurrentUser } from "./shared/models/current-user";
import { AccountService } from "./shared/services/account.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent implements OnInit {
  title = "Project Bajheera";

  constructor(private accountService: AccountService) {}

  ngOnInit(): void {
    this.setCurrentUser();
  }

  setCurrentUser(): void {
    const currentUser: CurrentUser | null = this.accountService.getCurrentUser();
    this.accountService.setCurrentUser(currentUser);
  }
}
