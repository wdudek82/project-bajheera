upstream backend {
  server backend:5001;
}

upstream frontend {
  server frontend:4200;
}

upstream socket {
 server frontend:4201;
}

server {
  client_max_body_size 50m;
  listen 80;
  access_log /var/log/nginx/access.log;
  access_log /var/log/nginx/error.log;

  location / {
    proxy_pass http://frontend;
    proxy_set_header        Host $http_host;
    proxy_set_header        X-Real-IP $remote_addr;
    proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header        X-Forwarded-Proto $scheme;

    client_max_body_size    50m;
    client_body_buffer_size 128k;
    proxy_connect_timeout   60s;
    proxy_send_timeout      90s;
    proxy_read_timeout      90s;
    proxy_buffering         off;
    proxy_temp_file_write_size 64k;
    proxy_redirect          off;

    ## websockets
    proxy_http_version      1.1;
    proxy_set_header        Upgrade $http_upgrade;
    proxy_set_header        Connection "upgrade";

    access_log /var/log/nginx/aspnet_access.log;
    error_log /var/log/nginx/aspnet_error.log;
  }

  location /api {
    rewrite /api(/.*)? $1 break;
    proxy_set_header        Host $http_host;
    proxy_set_header        X-Real-IP $remote_addr;
    proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header        X-Forwarded-Proto $scheme;

    client_max_body_size    50m;
    client_body_buffer_size 128k;
    proxy_connect_timeout   60s;
    proxy_send_timeout      90s;
    proxy_read_timeout      90s;
    proxy_buffering         off;
    proxy_temp_file_write_size 64k;
    proxy_pass              https://backend;
    proxy_redirect          off;

    ## websockets
    #proxy_http_version      1.1;
    #proxy_set_header        Upgrade $http_upgrade;
    #proxy_set_header        Connection "upgrade";
  }

}
