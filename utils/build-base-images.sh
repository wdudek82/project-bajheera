#!/usr/bin/env bash

CONTEXT_PATH=./docker/node
DOCKERFILE=Dockerfile

IMAGES=(
  base_node
)

for image_name in "${IMAGES[@]}"; do
  docker build -t wdudek82/bajheera-"${image_name}" -f ${CONTEXT_PATH}/${DOCKERFILE} ${CONTEXT_PATH}
done
